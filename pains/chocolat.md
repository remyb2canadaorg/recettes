# Pain de campagne

Source d'origine: [La petite bette](https://www.youtube.com/watch?v=qBgKWQ7Zrmg)

## Ingrédients

- 50g de farine d'épautre
- 450g de farine blanche, non blanchie
- 100g de levain naturel
- 30g de sirop d'érable
- 10g de sel
- 350g d'eau
- 15g d'eau pour l'après autolyse
- 15g de cacao
- 120g de pépites de chocolat

## En résumé

- **autolyse:** 30min
- **pétrissage:** 4 rabas à toute les 30min pendant 2h
- **1ère pousse (pointage):** nuit au frigo
- **2ème pousse (l'apprêt):** 30min
- **cuisson:** 45min avec couvercle, puis 15min sans couvercle, à 400 farenheight (205 celsius)

## Procédure

1. Combiner les farines, le levain, l'eau, et le sirop d'érable dans un bol
2. Mélanger à la main (2 minutes)
3. Autolyse de 30min (laisser reposer 30min)
4. Ajouter le sel et le 15g d'eau, mélanger jusqu'à ce que tous les ingrédients soient homogènes (un autre 2-3 minutes)
5. Faire un pétrissage "stretch and fold": à chaque 30min faire 4 rabats, pendant 2h.
6. Fleurer votre bol, faites une boule avec la pâte, et placez-là dans le bol
7. Placer le bol au frigo toute la nuit
8. Le matin, sortez le bol et laisser sur le contoir 30min
9. Étaler la pâte sur une table (environ 2-3cm d'épaisseur)
10. Poudrer la pâte de la poudre de cacao et des pépites
11. Replier la pâte sur elle même (4 fois: 2 fois sur la longueur, 2 fois sur la largeur)
12. Former une boule et laisser reposer pendant 30min sur le contoir (le temps que le four et la cocotte préchauffe)
13. Préchauffer le four à 400 fahrenheit (205 celsius) avec une cocotte préalablement placée à l'intérieur
14. Mettre la pâte dans la cocotte, [grigner](https://www.academiedugout.fr/articles/10-termes-a-connaitre-en-boulangerie_3415), et enfourner pour 45min (cocotte avec couvercle)
15. Retirer le couvercle de la cocotte  et continuer à cuire pour 15min
