# Pain de campagne

Source d'origine: 

## Ingrédients

- XXXg de farine blanche, non blanchie
- XXg de farine de seigle
- XXXg de levain naturel
- 10g de sel
- Xg de levure
- XXXg d'eau

## En résumé

- **pétrissage:** 
- **1ère pousse (pointage):** 
- **2ème pousse (l'apprêt):** 
- **cuisson:** 

## Procédure

1. Combiner les farines, le levain, la levure, et l'eau dans un bol
2. Mélanger à la main (2 minutes)
3. Rajouter le sel et continuer à mélanger jusqu'à ce que tous les ingrédients soient homogènes (un autre 2-3 minutes)
4. Pétrir la pâte jusqu'à ce qu'elle soit lisse (8-10 minutes)
5. Fleurer votre bol, faites une boule avec la pâte, et placez-là dans le bol
6. Placer le bol au frigo toute la nuit
7. Le matin, sortez le bol et laisser sur le contoir 45min
8. Façonner [le pâton](https://www.academiedugout.fr/articles/10-termes-a-connaitre-en-boulangerie_3415) en bâtard et reposer **XXX**
9. [Grigner](https://www.academiedugout.fr/articles/10-termes-a-connaitre-en-boulangerie_3415) et enfourner à **XXX** celicius pour **XXmin**
