# Recettes de pains

Ces recettes, venant de différentes sources, ont été modifiées pour avoir un temps de pointage la nuit (au frigo), utilisent toutes du levain naturel, et ont un temps de cuisson un peu plus long parce que mon four est nul.

- [Pain de campagne](campagne.md)
- [Pain de campagne à ma manière](campagne_remy.md)
- [Pain au chocolat](chocolat.md)
- [Pain Ekmek (pain turc)](ekmek.md)
- [Pain d'épautre](epautre.md)
- [Pains farcis](farcis.md)
- [Pain au fromage](fromage.md)
- [Pain à la meule](meule.md)
- [Pain au sarrasin](sarrasin.md)
- [Pain de tradition française](tradition_francaise.md)
- [Pain à l'avoine et au sirop d'érable](maple_oats.md)

## Levain naturel

Pour faire le levain naturel, j'utilise [cette méthode d'Éric Kayser](https://www.maison-kayser.com/info/maisonkayseracademy/recette-dun-levain-naturel-maison-secrets-deric-kayser/), avec de la farine de seigle.
Ça m'a pris un total de 8 jours à le faire naître.

L'idée générale est que vous avez toujours 2 types de levain:

1. un levain dit **"Chef"** que vous garder dans un pot, au frigo. C'est le levain de base que vous utiliserai afin de partir un autre levain pour les recettes: le levain "rafraîchit"
2. un levain dit **"Rafraîchit"** que vous garder dans un autre pot et que vous créé avant de faire votre pain.

La procédure que j'utilise pour préparer mon levain avant une recette est la suivante.

### Procédure pour obtenir du levain tout point

Si j'ai besoin de 100g de levain pour ma recette, je produirai 120g de levain (il y a toujours un peu de perte, je me donne 20g de "buffer"):

- La veille de la préparation du pain, je prends 15g de mon levain Chef que je mets dans un autre pot.
- J'y ajoute 15g d'eau de source et 15g de farine de seigle. Je mélange bien et je garde le pot sur le contoir toute la nuit. J'ai 60g de levain.
- Le matin, je rafraîchis mon 60g de levain avec 30g d'eau de source et 30g de farine de seigle. Je mélange bien et je garde le pot sur le contoir. J'ai 120g de levain.

Le levain devrait arriver "à point" (e.g. entre le double et le triple de son volume) dans les 4h - 6h.
Lorsque ça se produira, je suis prêts à faire mon pain.

Je pèse aussi mon pot de levain Chef lorsqu'il est vide (e.g. 407g). Comme ça, je sais combien de levain il me reste (e.g. si le pot fait 545g, je sais qu'il y a 498 - 407g = 91g de levain chef et, donc, je devrais le nourrir pour en faire d'autre).

## Astuces

### Le levain

Utiliser de la farine de seigle pour farie son levain (ou le rafraîchir) permet d'avoir un [temps de rafraîchi](https://www.painlevain.com/rafraichir-son-levain/) beaucoup plus rapide (environ 4 - 6h pour qu'il devienne tout point).

Ceci me donne mon ***levain chef*** que je garde au frigo et que je nourris soit lorsque ça fait une semaine, soit lorsqu'il y en a plus beaucoup.

Lorsque je prépare mon ***levain rafraîchi***, [j'entour mon bol avec un élastique](https://www.maryseetcocotte.com/wp-content/uploads/2018/01/levain.jpg). L'élastique me permet de visualiser aisément la poussée du levain.

### Le pétrissage manuel

J'utilise une méthode typique française pour pétrire ma pâte manuellement. Voir deux vidéo sur le sujet:

1. [École internationale de boulangerie](https://www.youtube.com/watch?v=FkvjdqkZWqg)
2. [Richard Bertinet](https://www.youtube.com/watch?v=cbBO4XyL3iM)

### La cuisson au four

Préchauffez votre four 30min avant d'y mettre le pain pour qu'il soit bien chaud .

Versez un verre d'eau dans le bas du four (e.g. dans un petit bol) lorsque le four est préchauffé. Ceci produira de la vapeur qui est nécessaire pour la pâte de doubler de volume lorsqu'elle cuit.

Utilisez une pierre à pizza, sans la huiler. Vous mettez la pierre dans le four (éteint), puis l'alumer pour qu'il se chauffe avec la pierre. La pierre sera très chaude lorsque vous mettrez la pâte dessus pour la cuire.

### La 2ème pousse (l'apprêt) et l'environnement

Le temps de fermentation [dépend de votre environnement](https://www.weekendbakery.com/posts/a-few-tips-on-dough-temperature/). S'il fait humide et chaud, le temps de fermentation sera moins long. S'il fait sec et froid, le temps sera plus long. Ceci car la température de fermentation d'une pâte est idéalement entre 23 et 24 celicius.

Les recettes ici se font avec un pointage au froid (1ère fermentation dans le frigo). Mais pour [l'apprêt](https://www.academiedugout.fr/articles/10-termes-a-connaitre-en-boulangerie_3415) (2ème fermentation avant l'enfournement), elle se fait sur le contoir.

Il est donc suggéré d'ajouter 30min ou de retirer 30min du temps d'apprêt pour chaque recette dépendant de la température de votre maison. I.e.:

- s'il fait moins de 20 degrés: augmenter le temps de fermentation de 30min
- s'il fait plus de 27 degrés: diminuer le temps de fermentation de 30min

### Si 1ère pousse (pointage) se fait le jour même

Si vous faites un pointage le jour même (donc pas au frigo durant toute la nuit), il est mieux de réchauffer votre eau à 30 degré celsius. Ceci pour avoir une pâte qui fermente autour de 23-24 degré celsius.

Un pointage de cette manière dure environ 2h.
