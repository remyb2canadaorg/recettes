# Pain de campagne

Source d'origine: un mélange des autres recettes.

## Ingrédients

- 50g de farine d'épautre
- 50g de farine de seigle
- 150g de farine [de meule](http://www.leschampsdelodie.fr/farinesdemeule/) (blé entier). [Vidéo sur la fabrication de la farine avec meule](https://www.youtube.com/watch?v=z4x6B8CQgzI)
- 250g de farine blanche, non blanchie
- 10g de sel
- 150g de levain naturel
- 2g de levure
- 350g d'eau

## En résumé

- **pétrissage:** 10min
- **1ère pousse (pointage):** toute la nuit au frigo
- **2ème pousse (l'apprêt):** 2h à 3h
- **cuisson:** 15min à 250 celsius, puis 20min à 230 celsius

## Procédure

1. Combiner les farines, le levain, la levure, et l'eau dans un bol
2. Mélanger à la main (2 minutes)
3. Rajouter le sel et continuer à mélanger jusqu'à ce que tous les ingrédients soient homogènes (un autre 2-3 minutes)
4. Pétrir la pâte jusqu'à ce qu'elle soit lisse (8-10 minutes)
5. Fleurer votre bol, faites une boule avec la pâte, et placez-là dans le bol
6. Placer le bol au frigo toute la nuit
7. Le matin, sortez le bol et laisser sur le contoir 45min
8. Façonner [le pâton](https://www.academiedugout.fr/articles/10-termes-a-connaitre-en-boulangerie_3415) en bâtard et reposer entre 2h et 3h
9. [Grigner](https://www.academiedugout.fr/articles/10-termes-a-connaitre-en-boulangerie_3415) et enfourner à 250 celsius pour 15min, puis à 230 celsius pour 20min
