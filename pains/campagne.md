# Pain de campagne

Source d'origine: [Éric Kayser](https://www.youtube.com/watch?v=WZ5o78rAZic)

## Ingrédients

- 450g de farine blanche, non blanchie
- 50g de farine de seigle
- 100g de levain naturel
- 10g de sel
- 2g de levure
- 340g d'eau

## En résumé

- **pétrissage:** 10min
- **1ère pousse (pointage):** 1h + nuit au frigo (avec un rabas à la 1ère heure)
- **2ème pousse (l'apprêt):** 1h30
- **cuisson:** 35min à 230 celsius

## Procédure

1. Combiner les farines, le levain, la levure, et l'eau dans un bol
2. Mélanger à la main (2 minutes)
3. Rajouter le sel et continuer à mélanger jusqu'à ce que tous les ingrédients soient homogènes (un autre 2-3 minutes)
4. Pétrir la pâte jusqu'à ce qu'elle soit lisse (8-10 minutes)
5. Fleurer votre bol, faites une boule avec la pâte, et placez-là dans le bol
6. Reposer 1h
7. Faites un rabat
8. Placer le bol au frigo toute la nuit
9. Le matin, sortez le bol et laisser sur le contoir 45min
10. Façonner [le pâton](https://www.academiedugout.fr/articles/10-termes-a-connaitre-en-boulangerie_3415) en bâtard et reposer 1h30
11. [Grigner](https://www.academiedugout.fr/articles/10-termes-a-connaitre-en-boulangerie_3415) et enfourner à 240 celsius pour 35min
