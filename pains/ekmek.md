# Pain de campagne

Source d'origine: [Éric Kayser](https://www.youtube.com/watch?v=8Xw_O4eYvkw)

## Ingrédients

- 500g de farine blanche, non blanchie
- 100g de levain naturel
- 10g de sel
- 4g de levure sèche ou instantanée
- 225g d'eau
- 75g de miel
- 40g d'huile d'olive

## En résumé

- **pétrissage:** 15min
- **1ère pousse (pointage):** la nuit au frigo
- **repos:** 30min
- **2ème pousse (l'apprêt):** 2h
- **cuisson:** 25min à 230 celsius

## Procédure

1. Combiner l'eau, la levure, la farine, le sel, le levain
2. Mélanger à la main (2 minutes)
3. Rajouter le miel et l'huile d'olive et continuer à mélanger jusqu'à ce que tous les ingrédients soient homogènes et que l'huile d'olive soit bien absorbée (un autre 2-3 minutes)
4. Pétrir la pâte jusqu'à ce qu'elle soit lisse (8-10 minutes)
5. Fleurer votre bol, faites une boule avec la pâte, et placez-là dans le bol
6. Placer le bol au frigo toute la nuit
7. Le matin, sortez le bol et diviser la pâte en 3 morceaux égaux
8. Faites une boule avec chaque morceaux et laisser reposer 30min
9. Dégaser légèrement et façonner [les pâtons](https://www.academiedugout.fr/articles/10-termes-a-connaitre-en-boulangerie_3415) en boule (une autre fois) en ramenant chaque bout sur le centre
10. Aplatisser un peu les boules, [grignez-les](https://www.academiedugout.fr/articles/10-termes-a-connaitre-en-boulangerie_3415) (e.g. en polka), et laisser fermenter pendant 2h
11. Huiler les boules légèrement, et enfourner à 230 celsius pour 25min
